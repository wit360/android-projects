package com.wit360.ktip.lib;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;


public class PantipConnector {

	public static String getTopicList(String room, int last_id_current_page, int current_page) {
		StringBuilder output = new StringBuilder();
		HttpPost httpPost = new HttpPost("http://pantip.com/forum/topic/ajax_json_all_topic_info_loadmore");
		httpPost.addHeader("X-Requested-With", "XMLHttpRequest");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>(2);
		params.add(new BasicNameValuePair("dataSend[topic_type][type]", "0"));
		params.add(new BasicNameValuePair("dataSend[topic_type][default_type]", "1"));
		params.add(new BasicNameValuePair("thumbnailview", 		  "false"));
		params.add(new BasicNameValuePair("dataSend[room]",       room));
		params.add(new BasicNameValuePair("last_id_current_page", ""+last_id_current_page));
		params.add(new BasicNameValuePair("current_page", 		  ""+current_page));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		HttpClient httpclient = new DefaultHttpClient();
		try {
			HttpResponse response = httpclient.execute(httpPost);
			BufferedReader in = new BufferedReader( new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			String temp;
			while ((temp = in.readLine()) != null) {
				output.append(temp);
			}
		} catch (ClientProtocolException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return output.toString();
	}

	/*
	Matcher m = patternDesc.matcher(linebuf);
	if (m.find()) {
		description = m.group(1);
        break;
	}
	*/
	public static String getTopicDetail(int topicid) {
		StringBuilder output = new StringBuilder();
		HttpGet httpGet = new HttpGet("http://pantip.com/topic/" + topicid);
        boolean isStartFooter  = false;
        HttpClient httpclient = new DefaultHttpClient();
		try {
			HttpResponse response = httpclient.execute(httpGet);
			BufferedReader in = new BufferedReader( new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			String 	linebuf;
			boolean isReadingStory = false; 
			while ((linebuf = in.readLine()) != null) {
				// System.out.println(linebuf); // NOTE: for debug
				if (isReadingStory) {
					// check for end
					int pos;
					if (linebuf.contains("<!-- Start footer -->")) {
						isStartFooter = true;
						break;
					} else if ((pos = linebuf.indexOf("<div class=\"edit-history\">")) != -1) {
						if (pos > 0) { output.append(linebuf.subSequence(0, pos)); }
						break;
					}
					output.append(linebuf + "\n");
				} else if (linebuf.contains("<div class=\"display-post-story\">")) {
					isReadingStory = true;
				}
			}
		} catch (ClientProtocolException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		if (isStartFooter) {
			return output.substring(9,output.length()-22);
		}
		return output.substring(9,output.length());
	}

	private static Pattern patternDesc = Pattern.compile("meta name=\"description\" content=\"(.*)\"");
	public static String getTopicShortDetail(int topicid) {
		String 	description = "";
		HttpGet httpGet = new HttpGet("http://pantip.com/topic/" + topicid);
        DefaultHttpClient httpclient = new DefaultHttpClient();
		try {
			HttpResponse response = httpclient.execute(httpGet);
			BufferedReader in = new BufferedReader( new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			String 	linebuf;
			while ((linebuf = in.readLine()) != null) {
                Matcher m = patternDesc.matcher(linebuf);
                if (m.find()) {
                    description = m.group(1);
                    break;
                }
			}
		} catch (ClientProtocolException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return description;
	}

	private static Pattern patternRoom      = Pattern.compile("href=\"/forum/(.*?)\" title=\"(.*?)\"");
	private static Pattern patternRoomTitle = Pattern.compile("<span class=\"title\">(.*?)</span>");
	private static ArrayList<Room> roomsBuffer = null;
	public static ArrayList<Room> getRoomList() {
		if (roomsBuffer!=null) {
			return roomsBuffer;
		}
		roomsBuffer = new ArrayList<>();
		HttpGet httpGet = new HttpGet("http://pantip.com/");
        DefaultHttpClient httpclient = new DefaultHttpClient();
		try {
			HttpResponse response = httpclient.execute(httpGet);
			BufferedReader in = new BufferedReader( new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			String 	linebuf;
			while ((linebuf = in.readLine()) != null) {
                Matcher m1 = patternRoom.matcher(linebuf);
                if (m1.find()) {
                	linebuf = in.readLine();
                	linebuf = in.readLine();
                	Matcher m2 = patternRoomTitle.matcher(linebuf);
                	if (m2.find()) {
                        roomsBuffer.add( new Room(m2.group(1), m1.group(2), m1.group(1),0) );
                	}
                } else if (linebuf.contains("panel #1")) {
                	break;
                }
			}
		} catch (ClientProtocolException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return roomsBuffer;
	}
}
