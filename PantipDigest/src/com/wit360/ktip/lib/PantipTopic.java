package com.wit360.ktip.lib;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PantipTopic {

	int 		id;
	String		disp_topic;
	String 		author;
	int 		comment;
	int 		vote;
	String 		utime;
	ArrayList<String> tags;
	
	public int getId() 			{ return id; }
	public String getTopic() 	{ return disp_topic; }
	public String getAuthor()	{ return author; }
	public int getComment() 	{ return comment; }
	public int getVote() 		{ return vote; }
	public String getUtime() 	{ return utime; }
	public ArrayList<String> getTags() { return tags; }

	public PantipTopic(int id, String dt, String a, int c, int v, String t, ArrayList<String> ts) {
		this.id 		= id;
		this.disp_topic = dt;
		this.author 	= a;
		this.comment 	= c;
		this.vote 		= v;
		this.utime 		= t;
		this.tags 		= ts;
	}
	
	public static PantipTopic createFromJSON(JSONObject topicjson) throws JSONException {
		// read tag list
		ArrayList<String> tags = new ArrayList<>();
		JSONArray tagjson = topicjson.getJSONArray("tags");
		for (int i=0; i<tagjson.length(); i++) {
			tags.add(tagjson.getJSONObject(i).getString("tag"));
		}

		// construct
		return new PantipTopic(
			topicjson.getInt("_id"),
			topicjson.getString("disp_topic"),
			topicjson.getString("author"),
			topicjson.getInt("comments"),
			topicjson.getInt("votes"),
			topicjson.getString("utime"),
			tags);
	}
	
	public static int parseAJAXTopic(ArrayList<PantipTopic> output, JSONObject json) throws JSONException {
		JSONArray topics = json.getJSONObject("item").getJSONArray("topic");
		int n = topics.length();
		for (int i=0; i<n; i++) {
			output.add(
				PantipTopic.createFromJSON(topics.getJSONObject(i))
			);
		}		
		return json.getJSONObject("item").getInt("last_id_current_page");
	}
	
	@Override public String toString() {
		return String.format("[%d / c:%2d v:%2d] %s // %s",
					getId(), getComment(), getVote(), getTopic(), getAuthor());
	}
}
