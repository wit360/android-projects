package com.wit360.ktip.lib;

import android.util.Log;

public class Room {
	public static int TRUNCATE_LEN = 60;
	String title;
	String subtitle;
	String path;
	String truncatedSubtitle;
	int    imageId;
	
	public Room(String t, String s, String p, int i) {
		title 		= t;
		subtitle 	= s;
		path 		= p;
		imageId 	= i;
		
		if (s.length() > TRUNCATE_LEN) {
			int pos = s.lastIndexOf(' ',TRUNCATE_LEN);
			if (pos > TRUNCATE_LEN-10) {
				truncatedSubtitle = s.substring(0, pos) + "...";
			} else {
				truncatedSubtitle = s.substring(0, TRUNCATE_LEN-3) + "...";
			}
			Log.i("room",truncatedSubtitle + " " + pos);
		} else {
			truncatedSubtitle = s;
		}
	}

	public String getTitle() { return title; }
	public String getSubtitle() { return subtitle; }
	public String getTruncatedSubtitle() { return truncatedSubtitle; }
	public String getPath() { return path; }
	public int getImageId() { return imageId; }
	
	@Override
	public String toString() {
		return String.format("%s - %s (%s) / img=%d",
				getTitle(), getSubtitle(), getPath(), getImageId());
	}
}
