package com.wit360.ktip.pantipdigest;

import java.util.ArrayList;

import com.wit360.ktip.lib.PantipConnector;
import com.wit360.ktip.lib.Room;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

public class RoomListFragment extends Fragment {

	GridView 		gvRoomList;
	ArrayList<Room> mRoomList;
	RoomClickListener mClickHandler;
	
	public interface RoomClickListener {
		public void onRoomClicked(Room r);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mClickHandler = (RoomClickListener)activity;
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		mClickHandler = null;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mRoomList = new ArrayList<>();
        new FetchRoomInfoTask().execute();
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_roomlist, container, false);
		gvRoomList = (GridView) v.findViewById(R.id.gridview_roomlist);
		gvRoomList.setAdapter(new RoomListAdapter(getActivity()));
		gvRoomList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int pos, long id) {
				mClickHandler.onRoomClicked(mRoomList.get(pos));
			}
		});
		return v;
	}
	
	class RoomListAdapter extends ArrayAdapter<Room> {
		public RoomListAdapter(Context context) {
			super(context, 0, mRoomList);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
	        // If we weren't given a view, inflate one
	        if (convertView == null) {
	            convertView = getActivity().getLayoutInflater().inflate(R.layout.griditem_room, null);
	        }

	        // Configure the view for this Crime
	        Room r = getItem(position);
	        TextView tvTitle = (TextView)convertView.findViewById(R.id.griditem_room_text_title);
	        tvTitle.setText(r.getTitle());
	        TextView tvSubtitle = (TextView)convertView.findViewById(R.id.griditem_room_text_subtitle);
	        tvSubtitle.setText(r.getTruncatedSubtitle());

	        return convertView;
		}
	}
	
	private class FetchRoomInfoTask extends AsyncTask<Void,Void,ArrayList<Room>> {
        @Override
        protected ArrayList<Room> doInBackground(Void... params) {
        	ArrayList<Room> output;
        	output = PantipConnector.getRoomList();
        	return output;
        }
        @Override
        protected void onPostExecute(ArrayList<Room> result) {
        	mRoomList.addAll(result);
        	RoomListAdapter rla = (RoomListAdapter)gvRoomList.getAdapter();
        	rla.notifyDataSetChanged();
        }
    }
}
