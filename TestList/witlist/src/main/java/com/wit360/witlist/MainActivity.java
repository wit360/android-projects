package com.wit360.witlist;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    ListView v;
    ArrayList<ColorListItem> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        items = new ArrayList<ColorListItem>();
        for (int i=0; i<100; i++) {
            items.add(new ColorListItem("Item " + (i*6+1), Color.rgb(81, 165, 186), 300));
            items.add(new ColorListItem("Item " + (i*6+2), Color.rgb(65, 146, 75), 200));
            items.add(new ColorListItem("Item " + (i*6+3), Color.rgb(135, 226, 147), 500));
            items.add(new ColorListItem("Item " + (i*6+4), Color.rgb(254, 132, 2), 400));
            items.add(new ColorListItem("Item " + (i*6+5), Color.rgb(107, 202, 226), 200));
            items.add(new ColorListItem("Item " + (i*6+6), Color.rgb(175, 234, 170), 300));
        }

        setContentView(R.layout.activity_main);
        v = (ListView) findViewById(R.id.listView_simple);
        v.setAdapter(new ColorfulTextAdapter(this,items));
    }

    class ColorListItem {
        public String text;
        public int bgcolor;
        public int height;
        public ColorListItem(String t, int c, int h) {
            text    = t;
            bgcolor = c;
            height  = h;
        }
    }

    class ColorfulTextAdapter extends ArrayAdapter<ColorListItem> {

        public ColorfulTextAdapter(Context context, List<ColorListItem> objects) {
            super(context, 0, objects);
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public View getView(int index, View convertView, ViewGroup viewGroup) {
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.list_item_simple,viewGroup,false);
            }
            assert convertView != null;
            TextView tv = (TextView) convertView.findViewById(R.id.textview_color);
            ColorListItem item = getItem(index);
            tv.setText(item.text);
            tv.setBackgroundColor(item.bgcolor);
            tv.setTextColor(Color.WHITE);
            tv.setPadding(10,10,10,item.height*2);
            return convertView;
        }
    }
}
